"""procesar_eval URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from api.views import ClientesViewSet, GruposViewSet, GrupoCuentasViewSet, \
			CuentaPagosViewSet, CuentasViewSet, TrnasaccionesViewSet
from rest_framework import routers


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'clientes', ClientesViewSet)
router.register(r'grupos', GruposViewSet)
router.register(r'cuentas', CuentasViewSet)
router.register(r'transacciones', TrnasaccionesViewSet)
router.register(r'grupos/(?P<grupo>\w{5})/cuentas', GrupoCuentasViewSet)
router.register(r'cuentas/(?P<cuenta>\w{5})/pagos', CuentaPagosViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),
    
]
