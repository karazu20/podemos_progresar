from django.shortcuts import render
from rest_framework import  status, viewsets
from rest_framework.decorators import api_view
from  api.serializers import ClientesSerializer, GruposSerializer, \
				CuentasSerializer, CalendarioPagosSerializer, TransaccionesSerializer
from api.models import CalendarioPagos, Clientes, Cuentas, Grupos, Transacciones
from rest_framework.response import Response
from django.db.models import Count

# Create your views here.

# ViewSets define the view behavior.
class ClientesViewSet(viewsets.ModelViewSet):
	queryset = Clientes.objects.all()
	serializer_class = ClientesSerializer


class GruposViewSet(viewsets.ModelViewSet):
	queryset = Grupos.objects.all()
	serializer_class = GruposSerializer


class CuentasViewSet(viewsets.ModelViewSet):
	queryset = Cuentas.objects.all()
	serializer_class = CuentasSerializer


class TrnasaccionesViewSet(viewsets.ModelViewSet):
	queryset = Transacciones.objects.all()
	serializer_class = TransaccionesSerializer



class GrupoCuentasViewSet(viewsets.ModelViewSet):	
	queryset = Cuentas.objects.all()
	serializer_class = CuentasSerializer

	def list(self, request, grupo):
		queryset = Cuentas.objects.filter(grupo=grupo)
		serializer = CuentasSerializer(queryset, many=True)
		return Response(serializer.data)

	def create(self, request, grupo):		
		serializer = CuentasSerializer(data=request.data)	
		if serializer.is_valid():	
			serializer.save()		
			return Response({'msg': 'Cuenta registrada con su calendario de pagos'}, status=status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)	


class CuentaPagosViewSet(viewsets.ModelViewSet):	
	queryset = Transacciones.objects.all()
	serializer_class = TransaccionesSerializer

	def list(self, request, cuenta):
		queryset = Transacciones.objects.filter(cuenta=cuenta)
		serializer = TransaccionesSerializer(queryset, many=True)
		return Response(serializer.data)

	def create(self, request, cuenta):		
		serializer = TransaccionesSerializer(data=request.data)	
		if serializer.is_valid():	
			serializer.save()		
			return Response({'msg': 'Pago registrado'}, status=status.HTTP_201_CREATED)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)	