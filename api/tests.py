from django.test import TestCase
import json
from rest_framework.test import APIClient


client = APIClient()

print ('-----------------------------Get clientes------------------------------')
response = client.get('http://127.0.0.1:8000/clientes/')
assert response.status_code == 200
print (json.dumps(response.data, indent=2))
print ('-----------------------------------------------------------------------')


print ('----------------------Get clientes por grupo---------------------------')
response = client.get('http://127.0.0.1:8000/grupos/')
assert response.data[0].get('clientes', False)
print (json.dumps(response.data, indent=2))
print ('-----------------------------------------------------------------------')


grupo_id = response.data[0].get('id', False)

print ('-----------------------Get cuentas por grupo {}-------------------------'\
	.format(grupo_id))
response = client.get('http://127.0.0.1:8000/grupos/{}/cuentas/'.format(grupo_id))
assert response.data[0].get('calendario_pagos', False) \
		and response.data[0].get('transacciones', False)
print (json.dumps(response.data, indent=2))
print ('------------------------------------------------------------------------')

saldo = response.data[0].get('saldo', False)
id_cuenta = response.data[0].get('id', False)


if saldo and id_cuenta:
	print ('----------------Registrar Pago en la cuenta {} con saldo: {}-------------'\
		.format(id_cuenta,saldo))
	data_pago = {'monto': saldo, 'cuenta':id_cuenta}
	response = client.post('http://127.0.0.1:8000/cuentas/{}/pagos/'.format(id_cuenta), \
		data_pago, format='json')
	assert response.status_code == 201
	
	response = client.get('http://127.0.0.1:8000/cuentas/{}/'.format(id_cuenta))
	assert response.status_code == 200 \
		and response.data['estatus']=='CERRADA'	

	print (json.dumps(response.data, indent=2))
print ('-------------------------------------------------------------------------')



if grupo_id:
	print ('-----------------Registrar Cuenta al grupo: {}-----------------------------'\
		.format(grupo_id))
	data_cuenta = {'monto': 100000, 'grupo':grupo_id}
	response = client.post('http://127.0.0.1:8000/grupos/{}/cuentas/'.format(grupo_id), \
		data_cuenta, format='json')
	assert response.status_code == 201
	print (json.dumps(response.data, indent=2))
	print ('---------------------------------------------------------------------------')