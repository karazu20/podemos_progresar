# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.utils import timezone
import uuid

from django.db.models.signals import post_save
from django.dispatch import receiver


from datetime import timedelta, date


ESTATUS_CUENTA = (
	('desembolsada','DESEMBOLSADA'),
	('cerrada','CERRADA'),
)

ESTATUS_PAGOS = (
	('pendiente','PENDIENTE'),
	('pagado','PAGADO'),
	('parcial','PARCIAL'),
	('atrasado','ATRASADO'),
)



class Clientes(models.Model):
	id = models.CharField(primary_key=True, max_length=7)
	nombre = models.CharField(max_length=60)


	class Meta:
		managed = False
		db_table = 'Clientes'


class Cuentas(models.Model):
	def get_pk ():		
		uid = uuid.uuid4()		
		new_pk = uid.hex[:5].upper()
		return new_pk

	id = models.CharField(primary_key=True, max_length=5, default=get_pk)
	grupo = models.ForeignKey('Grupos', models.DO_NOTHING)
	estatus = models.CharField(max_length=15, default='desembolsada', choices=ESTATUS_CUENTA)
	monto = models.DecimalField(max_digits=15, decimal_places=2)
	saldo = models.DecimalField(max_digits=15, decimal_places=2, default=0)

	

	class Meta:
		managed = False
		db_table = 'Cuentas'


class CalendarioPagos(models.Model):
	def get_pk ():		
		new_pk = CalendarioPagos.objects.filter().order_by('-id').first().id + 1		
		return new_pk

	id = models.IntegerField(primary_key=True, default=get_pk)
	cuenta = models.ForeignKey('Cuentas', models.DO_NOTHING)
	num_pago = models.IntegerField()
	monto = models.DecimalField(max_digits=15, decimal_places=2)
	fecha_pago = models.DateField()
	estatus = models.CharField(max_length=15, default='pendiente', choices=ESTATUS_PAGOS)

	class Meta:
		managed = False
		db_table = 'CalendarioPagos'


class Grupos(models.Model):
	id = models.CharField(primary_key=True, max_length=5)
	nombre = models.CharField(max_length=20)
	clientes = models.ManyToManyField(Clientes,  
		through='Miembros',
		through_fields=('grupo', 'cliente'),)

	class Meta:
		managed = False
		db_table = 'Grupos'


class Miembros(models.Model):
	
	grupo = models.ForeignKey(Grupos,  on_delete=models.CASCADE, db_column='grupo_id')
	cliente = models.ForeignKey(Clientes,  on_delete=models.CASCADE, db_column='cliente_id')

	class Meta:
		managed = False
		db_table = 'Miembros'
		unique_together = (('grupo', 'cliente'),)


class Transacciones(models.Model):
	
	def get_pk ():		
		new_pk = Transacciones.objects.filter().order_by('-id').first().id + 1		
		return new_pk


	id = models.IntegerField(primary_key=True, default=get_pk)
	cuenta = models.ForeignKey(Cuentas, models.DO_NOTHING)
	fecha = models.DateTimeField(default = timezone.now)
	monto = models.DecimalField(max_digits=15, decimal_places=2)


	class Meta:
		managed = False
		db_table = 'Transacciones'




@receiver(post_save, sender=Cuentas)
def init_cuentas(sender, **kwargs):	
	"""
		Al Crear cuenta se crea su calendario de Pagos
	"""				
	cuenta = kwargs['instance']
	if kwargs['created']:
		print ('-------Inicializando calendario de pagos-----------')
		monto = cuenta.monto
		monto_pago = monto/4
		residuo = monto%4		 
		fecha_pago = date.today()
		for i in range (1,5):			
			pago = CalendarioPagos()
			pago.cuenta = cuenta
			pago.num_pago = i

			if i < 4:
				pago.monto = monto_pago
			else:
				pago.monto = monto_pago + residuo			
			fecha_pago = fecha_pago + timedelta(days=7)
			pago.fecha_pago = fecha_pago
			print ('Se crea pago {} con monto {}'.format(pago.num_pago, pago.monto ))
			pago.save()
	