from rest_framework import serializers
from api.models import CalendarioPagos, Clientes, Cuentas, Grupos, Transacciones
			 

class ClientesSerializer(serializers.ModelSerializer):

	class Meta:
		model = Clientes
		fields = '__all__'



class CalendarioPagosSerializer(serializers.ModelSerializer):

	class Meta:
		model = CalendarioPagos
		fields = '__all__'

	

class TransaccionesSerializer(serializers.ModelSerializer):

	class Meta:
		model = Transacciones
		fields = ('monto', 'cuenta', 'fecha')

	def validate(self, data):
		"""
		Checa el saldo de la cuenta sea < al monto
		"""		
		saldo = Cuentas.objects.get(id=data['cuenta'].id).saldo
		if saldo < data['monto']:
			raise serializers.ValidationError("Monto del pago es mayor al saldo de la cuenta")
		return data


	def create(self, validated_data):		
		"""
		Actualiza el saldo y valida si el saldo queda en cero para Cerrar la cuenta
		"""		
		cuenta_id = validated_data['cuenta']		
		cuenta = Cuentas.objects.get(id=cuenta_id.id)
		saldo_nuevo = cuenta.saldo - validated_data['monto']
		cuenta.saldo = saldo_nuevo
		if cuenta.saldo <= 0:
			cuenta.estatus='CERRADA'
		cuenta.save()
		return Transacciones.objects.create(**validated_data)

	

class CuentasSerializer(serializers.ModelSerializer):
	calendario_pagos = CalendarioPagosSerializer(source='calendariopagos_set', many=True, 
					read_only=False, required=False) 
	transacciones = TransaccionesSerializer(source='transacciones_set', many=True, 
					read_only=False, required=False) 

	class Meta:
		model = Cuentas
		fields = ('id', 'grupo', 'saldo', 'monto', 'estatus', 'calendario_pagos', 'transacciones')
	
	def create(self, validated_data):		
		"""
		Agrega el Saldo igual al monto
		"""		
		monto = validated_data['monto']		
		validated_data['saldo'] = monto
		return Cuentas.objects.create(**validated_data)


class GruposSerializer(serializers.ModelSerializer):
	clientes = ClientesSerializer(many=True,read_only=True)	
	class Meta:
		model = Grupos
		fields = '__all__'

