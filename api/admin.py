from django.contrib import admin


from api.models import CalendarioPagos, Clientes, Cuentas, Grupos, Transacciones

# Register your models here.

admin.site.register(CalendarioPagos)
admin.site.register(Clientes)
admin.site.register(Cuentas)
admin.site.register(Grupos)
admin.site.register(Transacciones)
