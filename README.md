Prueba de Podemos Progresar
=============


# Prerequisitos

```
Mysql 8.0
Python 3.8.5
python3-dev default-libmysqlclient-dev build-essential

```


# Ejecución
Se divide el la preparación de la BD y la ejecución del API.

## Crear BD
El ejemplo esta hecho para hacerlo con un host de BD remoto, si se hace local omita el host y la var local_infile.

### Conectarte a tu consola mysql
```
$ mysql --local-infile=1 -u root -p'adminpass' -h 172.17.0.2 -P 3306
```

### Run script modelo_script.sql para crear la BD
```
mysql> source /dir_scripts/modelo_script.sql;
```

### Run script admin.sql para agregar el usuario y los permisos
```
mysql> source /dir_scripts/admin.sql;
```

### Carga de datos
Para la carga de datos asegurece de deshabilitar la propiedad secure-file-priv en el archivo /etc/mysql/my.cnf, porteriormente entre a mysql y prosiga

### Seleccionar la BD
```
mysql> use podemos_eval;
```

### Run script load_data.sql para cargar la información inicial, antes edite la ruta de los archivos en load_data.sql
```
mysql> source /dir_scripts/load_data.sql;
```


## Levantar Proyecto
El entorno virtual es opcional.

### Crear entorno virtaul
```
$ virtualenv -p python3 .env
$ source .env/bin/activate
```
### Instalar requerimientos python
```
$ pip install -r requirements.txt
```

### Sync Proyecto con BD, en el directorio del proyecto ejecutar
```
$ python manage.py makemigrations
$ python manage.py migrate
```

### Ejecutar proyecto
```
$ python manage.py runserver
```

### Ejecutar pruebas
```
$ python manage.py test
```






